# Maintainer: Stacy Harper <contact@stacyharper.net>
pkgname=kakoune-cr
pkgver=0.0.1
pkgrel=0
pkgdesc="kakoune.cr (kcr) is a command-line tool for Kakoune"
url="https://github.com/alexherbo2/kakoune.cr"
arch="x86_64 aarch64" # limited by crystal
license="MIT"
depends="jq"
makedepends="crystal shards"
_commit="43d4276e1d173839f335ff60f205b89705892e00"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/alexherbo2/kakoune.cr/archive/$_commit.tar.gz
	kcr.kak
"
builddir="$srcdir/kakoune.cr-$_commit"
subpackages="$pkgname-commands"

build() {
	make build
}

package() {
	install -Dm755 bin/kcr "$pkgdir"/usr/bin/kcr

	cd share/kcr
	find init/ pages/ -type f -exec install -Dm644 "{}" "$pkgdir/usr/share/kcr/{}" \;

	install -Dm644 applications/kcr.desktop "$pkgdir"/usr/share/applications/kcr.desktop

	mkdir -p "$pkgdir"/usr/share/kak/autoload/kcr
	install -Dm644 "$startdir"/kcr.kak "$pkgdir"/usr/share/kak/rc/kcr/kcr.kak

}

commands() {
	depends="$depends fd fzf"

	cd "$builddir"/share/kcr
	find commands/ -type f -name 'kcr-*' | while read file; do
		install -Dm755 "$file" "$subpkgdir/usr/bin/$(basename "$file")"
	done
}

sha512sums="
64708438936e71293a0f9191d6a21a4b5901bae17fd9c97e4be47ee67fe2f0eeb970c733b698ad89c7497d2586e14109a90da0cd06402fd9cf7c2b83b2dbbb15  kakoune-cr-0.0.1.tar.gz
5d44426f8829e07c34ac6e3b5a483f0103da5d9648d1705636df5f273bb35d81935474545240359455d6837461b54cef860f461ee52dfa3fddbf3233efded94f  kcr.kak
"
